import pygame
import pygame_menu
from pygame.locals import KEYDOWN, K_ESCAPE, K_LEFT, K_RIGHT, QUIT, K_SPACE, K_p, K_RETURN
from pygame.sprite import AbstractGroup

from level_generator import level_1, level_2, level_3, level_4, level_5
from templates import LevelTemplate

SCREEN_WIDTH = 640
SCREEN_HEIGHT = 480
SCREEN_BACKGROUND_COLOR = (40, 40, 40)

UP_RIGHT = pygame.Vector2(0.707, -0.707)

GREY_COLOR = (130, 130, 130)

class Player(pygame.sprite.Sprite):

    def __init__(self, movement_speed: int = 12, lives=3, *groups: AbstractGroup):
        super().__init__(*groups)
        self.surf = pygame.Surface((90, 10))
        self.surf.fill((240, 40, 0))
        self.rect = self.surf.get_rect()
        self.rect.move_ip(SCREEN_WIDTH / 2, SCREEN_HEIGHT * 0.8)
        self.movement_speed = movement_speed
        self.lives = lives
        self.should_spawn_ball = False

    def handle_pressed_keys(self, pressed_keys):

        if pressed_keys[K_LEFT]:
            self.rect.move_ip(-self.movement_speed, 0)
        if pressed_keys[K_RIGHT]:
            self.rect.move_ip(self.movement_speed, 0)

        if pressed_keys[K_SPACE]:
            self.should_spawn_ball = True
        else:
            self.should_spawn_ball = False

        if self.rect.left < 0:
            self.rect.left = 0
        if self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH

    def requested_to_spawn_ball(self):
        return self.should_spawn_ball

    def lose_life(self):
        self.lives -= 1

    def is_dead(self):
        return self.lives == 0


class Block(pygame.sprite.Sprite):

    def __init__(self, position_x, position_y, width, height, color=(80, 80, 190), *groups: AbstractGroup):
        super().__init__(*groups)
        self.surf = pygame.Surface((width, height))
        self.surf.fill(color)
        self.rect = self.surf.get_rect()
        self.rect.move_ip(position_x, position_y)


class Ball(pygame.sprite.Sprite):

    def __init__(self, position_x=SCREEN_WIDTH / 2, position_y=SCREEN_HEIGHT * 0.75, movement_speed: int = 5.5):
        self.surf = pygame.Surface((10, 10))
        self.surf.fill((240, 220, 220))
        self.rect = self.surf.get_rect()
        self.rect.move_ip(position_x, position_y)
        self.direction = pygame.Vector2(UP_RIGHT.x, UP_RIGHT.y)
        self.movement_speed = movement_speed

    def is_touching_bottom(self) -> bool:
        return self.rect.y >= SCREEN_HEIGHT * 0.95

    def update(self, player):
        if self.rect.colliderect(player) and self.rect.y <= 380:
            self.direction = (pygame.Vector2(self.rect.center) - pygame.Vector2(player.rect.center)).normalize()

        if self.rect.left < 0:
            self.rect.left = 0
            self.direction.x = -self.direction.x
        if self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH
            self.direction.x = -self.direction.x
        if self.rect.top < 0:
            self.rect.top = 0
            self.direction.y = -self.direction.y

        self.rect.move_ip(self.direction * self.movement_speed)

    def handle_hitting_blocks(self, blocks):
        block = pygame.sprite.spritecollideany(self, blocks)
        if block:
            block_center = pygame.Vector2(block.rect.center)
            ball_center = pygame.Vector2(self.rect.center)
            direction = (ball_center - block_center).normalize()
            if abs(direction.x) > abs(direction.y):
                self.direction.x = -self.direction.x
            else:
                self.direction.y = -self.direction.y

            blocks.remove(block)

            pygame.mixer.Channel(1).set_volume(0.2)
            pygame.mixer.Channel(1).play(pygame.mixer.Sound("sounds/blop.wav"))
            return True
        else:
            return False


def generate_level(template: LevelTemplate) -> AbstractGroup:
    width = template.width
    height = template.height
    blocks = pygame.sprite.Group()
    block_width = 30
    block_height = 30
    horizontal_block_margin = 5
    vertical_block_margin = 5
    horizontal_margin = (SCREEN_WIDTH - (width * block_width) - ((width - 1) * horizontal_block_margin)) / 2
    vertical_margin = 0.05 * SCREEN_HEIGHT
    for j in range(width):
        for i in range(height):
            block_template = template.blocks[i * width + j]
            position_x = horizontal_margin + (j - 1) * (block_width + horizontal_block_margin) + block_width
            position_y = vertical_margin + (i - 1) * (block_height + vertical_block_margin) + block_height
            block = Block(position_x, position_y, block_width, block_height, color=block_template.color)
            blocks.add(block)
    return blocks


def main():
    pygame.init()
    screen = pygame.display.set_mode([SCREEN_WIDTH, SCREEN_HEIGHT])
    clock = pygame.time.Clock()
    pygame.display.set_caption("Arkanoid UJ")
    font = pygame.font.SysFont(pygame.font.get_default_font(), 24)
    large_font = pygame.font.SysFont(pygame.font.get_default_font(), 80)
    should_run = True

    pygame.mixer.music.load("music/chocolate.mp3")
    pygame.mixer.music.set_volume(0.1)
    pygame.mixer.music.play(-1)

    player = create_player()
    ball = None
    blocks = None
    scores = []
    levels_queue = create_levels()
    current_level = -1
    score = 0
    is_paused = False
    show_end = False
    pressed_enter = False

    menu = pygame_menu.Menu("Main Menu", SCREEN_WIDTH, SCREEN_HEIGHT, theme=pygame_menu.themes.THEME_DARK)
    menu.add.button("Play", pygame_menu.events.CLOSE)
    menu.add.button("Quit", pygame_menu.events.EXIT)
    menu.add.button("High scores", scores_menu(), button_id="scores")
    menu.set_onclose(lambda m: m.disable())

    while should_run:
        screen.fill(SCREEN_BACKGROUND_COLOR)

        events = pygame.event.get()
        for event in events:
            if event.type == QUIT:
                should_run = False
            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    should_run = False
                elif event.key == K_p:
                    is_paused = not is_paused
                elif show_end and event.key == K_RETURN:
                    pressed_enter = True

        if menu.is_enabled():
            menu.draw(screen, True)
            menu.update(events)
        else:
            if show_end:
                you_won_label = large_font.render("YOU WON!", True, (255, 255, 255))
                screen.blit(you_won_label, (SCREEN_WIDTH * 0.25, SCREEN_HEIGHT * 0.4))
                paused_label_2 = font.render("press ENTER to proceed to menu", True, (255, 255, 255))
                screen.blit(paused_label_2, (SCREEN_WIDTH * 0.3, SCREEN_HEIGHT * 0.53))
                if pressed_enter:
                    scores.append(score)
                    score = 0
                    refresh_high_scores(menu, scores)
                    player = create_player()
                    scores = []
                    levels_queue = create_levels()
                    current_level = -1
                    is_paused = False
                    pressed_enter = False
                    show_end = False
                    ball = None
                    blocks = None
                    menu.enable()
            else:
                if not blocks:
                    current_level += 1
                    if current_level == len(levels_queue):
                        show_end = True
                    else:
                        blocks = generate_level(levels_queue[current_level])
                    ball = None

                pressed_keys = pygame.key.get_pressed()

                if not is_paused:
                    player.handle_pressed_keys(pressed_keys)
                if player.requested_to_spawn_ball() and not ball and not is_paused:
                    ball = Ball(position_x=player.rect.x)

                if ball:
                    is_hit = ball.handle_hitting_blocks(blocks)
                    if is_hit:
                        score += 1
                    if not is_paused:
                        ball.update(player)
                    screen.blit(ball.surf, ball.rect)
                    if ball.is_touching_bottom():
                        player.lose_life()
                        ball = None

                if player.is_dead():
                    scores.append(score)
                    score = 0
                    refresh_high_scores(menu, scores)
                    menu.enable()
                    player = create_player()
                    scores = []
                    levels_queue = create_levels()
                    current_level = -1
                    ball = None
                    blocks = None
                    is_paused = False

                render_blocks(blocks, screen)
                render_game_labels(current_level, font, player, score, screen)
                render_throw_ball_label(font, screen)
                render_pause_labels(font, is_paused, large_font, screen)
                screen.blit(player.surf, player.rect)

        clock.tick(60)
        pygame.display.flip()

    pygame.quit()


def render_blocks(blocks, screen):
    if blocks:
        for block in blocks:
            screen.blit(block.surf, block.rect)


def render_throw_ball_label(font, screen):
    pause_label = font.render("press SPACE to throw ball", True, (255, 255, 255))
    screen.blit(pause_label, (SCREEN_WIDTH * 0.1, SCREEN_HEIGHT * 0.9))


def render_pause_labels(font, is_paused, large_font, screen):
    if not is_paused:
        pause_label = font.render("press P to pause", True, (255, 255, 255))
        screen.blit(pause_label, (SCREEN_WIDTH * 0.1, SCREEN_HEIGHT * 0.94))
    if is_paused:
        paused_label = large_font.render("PAUSED", True, (255, 255, 255))
        screen.blit(paused_label, (SCREEN_WIDTH * 0.35, SCREEN_HEIGHT * 0.4))
        paused_label_2 = font.render("press P to unpause", True, (255, 255, 255))
        screen.blit(paused_label_2, (SCREEN_WIDTH * 0.4, SCREEN_HEIGHT * 0.53))


def render_game_labels(current_level, font, player, score, screen):
    level_label = font.render(f"Level: {current_level + 1}", True, (255, 255, 255))
    screen.blit(level_label, (SCREEN_WIDTH * 0.8, SCREEN_HEIGHT * 0.9))
    score_label = font.render(f"Score: {score}", True, (255, 255, 255))
    screen.blit(score_label, (SCREEN_WIDTH * 0.8, SCREEN_HEIGHT * 0.94))
    lives_label = font.render(f"Lives: {player.lives}", True, (255, 255, 255))
    screen.blit(lives_label, (SCREEN_WIDTH * 0.8, SCREEN_HEIGHT * 0.86))


def create_levels():
    return [level_1(), level_2(), level_3(), level_4(), level_5()]


def create_player():
    return Player(lives=3)


def refresh_high_scores(menu, scores):
    menu.remove_widget(menu.get_widget("scores"))
    new_menu = scores_menu()
    scores.sort(reverse=True)
    for i, s in enumerate(scores):
        new_menu.add.label(f"{i + 1}. {s}")
    menu.add.button("High Scores", new_menu, button_id="scores")


def scores_menu():
    return pygame_menu.Menu("High Scores", SCREEN_WIDTH, SCREEN_HEIGHT, theme=pygame_menu.themes.THEME_DARK)


if __name__ == '__main__':
    main()
