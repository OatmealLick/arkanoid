from random import randint
from typing import List

from templates import BlockTemplate, LevelTemplate

GREEN_COLOR = (70, 220, 70)
RED_COLOR = (220, 70, 70)
BLUE_COLOR = (60, 60, 230)

# This could be also done with csv or json, but since the levels do not contain much information are fairly light on mem
# and it is way easier to iterate on project having them in memory, I decided to use them like this instead of files.

def append_row(width, color, blocks):
    for i in range(width):
        blocks.append(BlockTemplate(color))


def level_1():
    blocks = []
    width = 8
    height = 2
    append_row(width, GREEN_COLOR, blocks)
    append_row(width, GREEN_COLOR, blocks)
    return LevelTemplate(blocks, width, height)


def level_2():
    blocks: List[BlockTemplate] = []
    width = 5
    height = 4
    append_row(width, RED_COLOR, blocks)
    append_row(width, RED_COLOR, blocks)
    append_row(width, RED_COLOR, blocks)
    append_row(width, RED_COLOR, blocks)
    level = LevelTemplate(blocks, width, height)
    return level


def level_3():
    blocks: List[BlockTemplate] = []
    colors_to_choose_from = [RED_COLOR, GREEN_COLOR]
    width = 9
    height = 5
    for j in range(width):
        for i in range(height):
            random_color = colors_to_choose_from[randint(0, 1)]
            blocks.append(BlockTemplate(random_color))
    level = LevelTemplate(blocks, width, height)
    return level


def level_4():
    blocks: List[BlockTemplate] = []
    colors_to_choose_from = [BLUE_COLOR, GREEN_COLOR]
    width = 10
    height = 5
    for j in range(width):
        for i in range(height):
            random_color = colors_to_choose_from[randint(0, 1)]
            blocks.append(BlockTemplate(random_color))
    level = LevelTemplate(blocks, width, height)
    return level


def level_5():
    blocks: List[BlockTemplate] = []
    colors_to_choose_from = [BLUE_COLOR, GREEN_COLOR, RED_COLOR]
    width = 12
    height = 3
    append_row(width, BLUE_COLOR, blocks)
    for j in range(width):
        for i in range(height):
            random_color = colors_to_choose_from[randint(0, 2)]
            blocks.append(BlockTemplate(random_color))
    append_row(width, BLUE_COLOR, blocks)
    level = LevelTemplate(blocks, width, height + 2)
    return level
