from dataclasses import dataclass
from typing import List, Tuple


@dataclass
class BlockTemplate:
    color: Tuple[int, int, int]

    def __init__(self, color: Tuple[int, int, int]):
        self.color = color


@dataclass
class LevelTemplate:
    blocks: List[BlockTemplate]
    width: int
    height: int

    def __init__(self, blocks: List[BlockTemplate], width: int, height: int):
        self.width = width
        self.height = height
        self.blocks = blocks
